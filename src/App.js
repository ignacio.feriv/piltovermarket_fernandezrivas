import React, { Fragment } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { default as Navbar } from './components/Navbar/Navbar';
import ItemListContainer from './components/ItemListContainer/ItemListContainer';
import ItemDetailContainer from './components/ItemDetailContainer/ItemDetailContainer';
import styled from 'styled-components';

const Container = styled.div`
background-color: #F7F7F7;
width: 100%;
height: 100%;
`;

function App() {
  return (
    <BrowserRouter>
      <Fragment>
      <Container> 
        <Navbar/>
          <Routes>
            <Route exact path="/" element={<ItemListContainer greeting="Welcome to Piltover Market!"/>}/>
            <Route exact path="/category/:id" element={<ItemListContainer greeting="Welcome to Piltover Market!"/>}/>
            <Route exact path="/item/:id" element={<ItemDetailContainer/>}/>
          </Routes>
        </Container>
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
