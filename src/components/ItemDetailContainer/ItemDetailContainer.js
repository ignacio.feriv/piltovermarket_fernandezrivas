import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom";
import ItemDetail from '../ItemDetail/ItemDetail'

function ItemDetailContainer (props) {
    const [itemDetail, setItemDetail] = useState({})
    const { id } = useParams()

    useEffect(() => {
        const getItemDetail = new Promise( (resolve, reject) => {
            setTimeout(() => {
                resolve([
                    {
                        id: 1,
                        categoryId: 1,
                        title: "Botines Messi",
                        description: "Son los botines de Messi", 
                        price: "9999",
                        pictureUrl: "https://static.foxdeportes.com/2021/06/botines-messi-1.jpg"
                    },
                    {
                        id: 2,
                        categoryId: 1,
                        title: "Botines Neymar",
                        description: "Son los botines de Neymar", 
                        price: "9999",
                        pictureUrl: "https://images.ole.com.ar/2021/09/10/l6w6jyZPb_1200x630__1.jpg"
                    },
                    {
                        id: 3,
                        categoryId: 1,
                        title: "Botines Mbappe",
                        description: "Son los botines de Mbappe", 
                        price: "9999",
                        pictureUrl: "https://img.planetafobal.com/2020/08/botines-nike-mercurial-rosa-kylian-mbappe-2020-iu.jpg"
                    },
                    {
                        id: 4,
                        categoryId: 2,
                        title: "Camiseta Messi PSG 2021",
                        description: "Camiseta Messi 2021 Paris Saint Germain",
                        price: "12500",
                        pictureUrl: "https://images.ole.com.ar/2021/08/10/WenFNXDxU_720x0__1.jpg"
                    },
                    {
                        id: 5,
                        categoryId: 2,
                        title: "Camiseta Adidas AFA",
                        description: "Camiseta Adidas AFA Argentina Campeon Copa America 2021",
                        price: "13000",
                        pictureUrl: "https://www.dexter.com.ar/on/demandware.static/-/Sites-dabra-catalog/default/dwa344b913/products/AD_FS6565P/AD_FS6565P-1.JPG"
                    },
                    {
                        id: 6,
                        categoryId: 2,
                        title: "Botines Nike Mercurial",
                        description: "Botines Nike Mercurial Futbol 11",
                        price: "17000",
                        pictureUrl: "https://www.futbolemotion.com/imagescms/postblogs/16113/grandes/2016_Mercurial_Superfly_V_Total_Crimson_Black_Yellow_hd_1600.jpg"
                    },
                    {
                        id: 7,
                        categoryId: 2,
                        title: "Camiseta Liverpool 2021",
                        description: "Camiseta Liverpool 2021 Premier League",
                        price: "14000",
                        pictureUrl: "https://sporting.vteximg.com.br/arquivos/ids/344505-1500-1500/4DB2560-688-1.jpg?v=637648320066030000"
                    }
                ].filter(i => i.id === parseInt(id))[0]) 
            }, 2000)
        }) 
    
        getItemDetail.then(item => setItemDetail(item))
    }, [id])

    return (
        <ItemDetail props={itemDetail}/>
    )
}

export default ItemDetailContainer