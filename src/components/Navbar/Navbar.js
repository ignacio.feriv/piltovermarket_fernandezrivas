import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import CartWidget from '../CartWidget/CartWidget';

const Components = {
    Nav: ({children}) => <Nav className="me-auto">{ children }</Nav>,
    NavDropdown: ({children}) => <NavDropdown title="Categories" id="basic-nav-dropdown">{ children }</NavDropdown>,
    NavDropdownItems: () => {
        return (
            <>
                <NavDropdown.Item>
                    <Link to={`/category/1`}>Football Boots</Link>
                </NavDropdown.Item>
                <NavDropdown.Item>
                    <Link to={`/category/2`}>Football Shirts</Link>
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item>
                    <Link to={`/`}>View All</Link>
                </NavDropdown.Item>
            </>
        )
    }
}

function AppNavbar() {

    return (
        <div>
            <Navbar bg="dark" variant='dark' expand="lg">
                <Container>
                    <Navbar.Brand>
                        <Link to={"/"} style={{ textDecoration: 'none', color: 'inherit' }}>Piltover Market</Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Components.Nav>
                        <Components.NavDropdown>
                            <Components.NavDropdownItems/>
                        </Components.NavDropdown>
                        <Nav.Link>
                            <CartWidget/>
                        </Nav.Link>
                    </Components.Nav>
                    </Navbar.Collapse>
                    
                </Container>
            </Navbar>
        </div>
    )
}

export default AppNavbar