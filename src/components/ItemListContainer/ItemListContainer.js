import ItemList from "../ItemList/ItemList"
import styled from 'styled-components';

const Container = styled.div`
margin: 15px;
text-align: center;
`;
const GreetingContainer = styled.div`
text-align:center;
position:relative;
`;
const Greeting = styled.h1`
font-weight: 300;
padding: 20px;
border-radius: 5px;
border: 1px solid grey;
`

function ItemListContainer(props) {
    const { greeting } = props;
    return(
       <Container>
           <GreetingContainer>
                <Greeting>{greeting}</Greeting>
           </GreetingContainer>
           <ItemList/>
       </Container>
    )
}

export default ItemListContainer