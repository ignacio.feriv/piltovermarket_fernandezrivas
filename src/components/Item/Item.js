import { Link } from 'react-router-dom';
import ItemCount from '../ItemCount/ItemCount';
import styled from 'styled-components';

const Card = styled.div`
background-color: white;
border-radius: 5px;
margin: 30px;
text-align: center;
padding:10px;
`;
const Image = styled.img`
width: 100%;
height: 30vh;
object-fit: contain;
border-bottom: solid 1px lightgrey;
`;
const Title = styled.h2`
font-weight: 200;
`;
const Description = styled.p`
font-weight: 100;
margin:5px;
`;
const Price = styled.span`
font-weight: 400;
font-size: 30px;
`;

function Item({id, title, description, price, pictureUrl}) {
    return (
    <Card key={id}>
        <Link to={`/item/${id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
            <Image src={pictureUrl} alt={title}/>
            <Title>{title}</Title>
            <Description>{description}</Description>
            <Price>{`$${price}`}</Price>
        </Link>
    </Card>
    )
}

export default Item