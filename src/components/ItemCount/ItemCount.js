import {useState, useEffect} from 'react'
import styled from 'styled-components';

const ItemCountContainer = styled.div`
align-items: center;
text-align:center;
margin-bottom:0;
`;
const AmountContainer = styled.div`
display: inline-block;
align-items: center;
font-weight: 700;
`;
const AmountPicker  = styled.div`
display:flex;
align-items:center;
`;
const Remove = styled.button`
padding: 5px;
background-color: Transparent;
font-size: 25px;
border: none;
cursor: pointer;
font-weigth 700;
&:hover {
    color: red
}
`;
const Amount = styled.span`
width: 30px;
height: 30px;
border-radius: 10px;
border: 1px solid teal;
display: flex;
align-items: center;
justify-content: center;
margin: 0px 5px;
padding: 15px;
`;
const Add = styled.button`
padding: 5px;
background-color: Transparent;
font-size: 25px;
border: none;
cursor: pointer;
font-weigth 700;
&:hover {
    color: green
}
`;
const AddToCart = styled.button`
margin-left: 60px;
padding: 10px;
border: 1px solid teal;
background-color: white;
cursor: pointer;
font-weight: 500;
&:hover {
    background-color: #f9f4f4
};
font-size: 14px;
`;

const AvailableStock = styled.p`
    color: grey;
    font-weight: 100;
    display:flex;
    font-size:14px;
`

function ItemCount ({ stock, initial }) {
    let [selectedStock, setSelectedStock] = useState(initial)
    let [availableStock] = useState(stock)

    useEffect(()=> {

    }, [stock])

    const handleAddStock = () => (selectedStock+1) <= availableStock ? setSelectedStock(selectedStock+1) : null
    const handleSubStock = () => (selectedStock-1) >= 1 ? setSelectedStock(selectedStock-1) : null
    const handleClick = () => {
        const onAddEvent = new CustomEvent('onAdd', {'detail': {'selectedStock': selectedStock}})
        window.dispatchEvent(onAddEvent)
    }


    return (
        <ItemCountContainer>
            <AmountContainer>
                <AmountPicker>
                    <Remove onClick={handleSubStock} >-</Remove>
                    <Amount>{selectedStock}</Amount>
                    <Add onClick={handleAddStock}>+</Add>
                </AmountPicker>
                <AvailableStock>({availableStock} units available)</AvailableStock>
            </AmountContainer>
            <AddToCart onClick={handleClick}>ADD TO CART</AddToCart>
        </ItemCountContainer>
    )
}

export default ItemCount