import React, { useState }  from 'react'
import { Link } from 'react-router-dom';
import styled from 'styled-components'
import ItemCount from '../ItemCount/ItemCount';

const Container = styled.div`
margin: 40px;
background-color: white;
border-radius: 5px;
padding: 10px;
`;
const Wrapper = styled.div`
padding: 50;
display: flex;
`;
const ImgContainer = styled.div`
flex: 1;
`;
const Image = styled.img`
width: 100%;
height: 90vh;
object-fit: cover
`;
const InfoContainer = styled.div`
flex: 1;
padding: 0 50px;
`;
const Title = styled.h1`
font-weight: 200;
`;
const Description = styled.p`
margin: 20px 0px;
`;
const Price = styled.span`
font-weight: 100;
font-size: 40px
`;
const FilterContainer = styled.div`
width: 50%;
margin: 30px 0px;
display: flex;
justify-content: space-between;
`;
const Filter = styled.div`
display: flex;
align-items: center
`;
const FilterTitle = styled.span`
font-size: 20px;
font-weight: 200;
`;
const FilterColor = styled.div`
width: 20px;
height: 20px;
border-radius: 50%;
background-color: ${props => props.color};
margin: 0 5px;
cursor: pointer;
`;
const FilterSize = styled.select`
margin-left: 10px;
padding: 5px
`;
const FilterSizeOption = styled.option``;
const ItemCountContainer = styled.div`
width: 500px;
align-items: center;
`;
const AddedToCart = styled.p`
color: #05d605;
font-weight: 500;
`;
const CheckoutButton = styled.button`
margin-top: 15px;
padding: 10px;
border: 1px solid teal;
border-radius: 5px;
background-color: #cdf9cd;
cursor: pointer;
font-weight: 500;
&:hover {
    background-color: #9bf39b
};
font-size: 14px;
`;


function ItemDetail({props}) {
    const [addedToCart, setAddedToCart] = useState(false)
    let { title, description, price, pictureUrl } = props

    window.addEventListener('onAdd', (evt) => {
        evt.stopPropagation()
        console.log('Quantity added to cart: ',evt.detail.selectedStock)
        setAddedToCart(true)
    })

    return (
        <Container>
            <Wrapper>
                <ImgContainer>
                    <Image src={pictureUrl} />
                </ImgContainer>
                <InfoContainer>
                    <Title>{title}</Title>
                    <Description>{description}</Description>
                    <Price>${price}</Price>
                    <FilterContainer>
                        <Filter>
                            <FilterTitle>Color</FilterTitle>
                            <FilterColor color='black'/>
                            <FilterColor color='darkblue'/>
                            <FilterColor color='grey'/>
                        </Filter>
                        <Filter>
                            <FilterTitle>Size</FilterTitle>
                            <FilterSize>
                                <FilterSizeOption>XS</FilterSizeOption>
                                <FilterSizeOption>S</FilterSizeOption>
                                <FilterSizeOption>M</FilterSizeOption>
                                <FilterSizeOption>L</FilterSizeOption>
                                <FilterSizeOption>XL</FilterSizeOption>
                            </FilterSize>
                        </Filter>
                    </FilterContainer>
                    <ItemCountContainer>
                        {addedToCart ? <>
                                        <AddedToCart>Added to cart!</AddedToCart>
                                        <CheckoutButton>
                                            <Link to='/cart' style={{ textDecoration: 'none', color: 'inherit' }}>CHECKOUT</Link>
                                        </CheckoutButton></> : <ItemCount stock={5} initial={1}/>
                                        }
                    </ItemCountContainer>

                </InfoContainer>
            </Wrapper>
        </Container>
    )
}

export default ItemDetail