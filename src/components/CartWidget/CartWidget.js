import { Link } from 'react-router-dom'
import styled from 'styled-components';
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

const CartCointainer = styled.div`
text-decoration: none;
`;

function CartWidget() {
    return(
        <CartCointainer>
            <Link to={"/cart"} style={{ textDecoration: 'none', color: 'inherit' }}>
                <ShoppingCartIcon/> {" "} 4
            </Link>
        </CartCointainer>
    )
}

export default CartWidget